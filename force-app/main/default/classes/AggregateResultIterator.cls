global class AggregateResultIterator implements Iterator<AggregateResult> {
    
    private List<AggregateResult> results { get; set; }

    // tracks which result item is returned
    private Integer index { get; set; }
         
    global AggregateResultIterator(String query) {
        this.index = 0;
        this.results = Database.query(query);            
    } 
  
    global boolean hasNext(){ 
        return results != null && !results.isEmpty() && index < results.size(); 
    }    
  
    global AggregateResult next(){        
        return results[index ++];            
    }       
}