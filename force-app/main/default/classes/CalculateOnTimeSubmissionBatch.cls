public class CalculateOnTimeSubmissionBatch implements Database.Batchable<AggregateResult> {

    public Iterable<AggregateResult> start(Database.BatchableContext BC) {
        //get the first month of the fiscal year
        Integer orgFiscalMonth = [SELECT FiscalYearStartMonth FROM Organization].FiscalYearStartMonth;
        Date currentDate = System.today();

        //get start date of the fiscal year
        Integer fiscalYr = orgFiscalMonth > currentDate.month() ? currentDate.year() -1 : currentDate.year();
        Date orgFiscalYrStartDate = Date.newinstance(fiscalYr, orgFiscalMonth, 1);
        //replace the appended 00:00 on the date
        String startDateStr = String.valueOf(orgFiscalYrStartDate).replace(' 00:00', '');
        String endDateStr = String.valueOf(currentDate).replace(' 00:00', '');

        String query = 'SELECT COUNT(Id) cnt, Submitted_On_Time__c submittedOnTime, Team_Member__r.User__r.Id userId ' +
                       'FROM Timesheet_Entry__c ' + 
                       'WHERE Timesheet_Date__c >= ' + startDateStr + ' ' +
                           'AND Timesheet_Date__c <= ' + endDateStr + ' ' +
                       'GROUP BY Submitted_On_Time__c, Team_Member__r.User__r.Id ' +
                       'ORDER BY Team_Member__r.User__r.Id';

        System.debug('--- CalculateOnTimeSubmissionBatch qry: ' + query);
        return new AggregateResultIterable(query);
    }

    public void execute(Database.BatchableContext BC, List<sObject> scope) {
        Map<Id, List<AggregateResult>> resultByUserId = new Map<Id, List<AggregateResult>>();

        for (SObject sObj : scope) {
            AggregateResult ar = (AggregateResult) sObj;
            Id memberId = (Id) ar.get('userId');
            List<AggregateResult> objList = resultByUserId != null && resultByUserId.containsKey(memberId) ? resultByUserId.get(memberId) : new List<AggregateResult>();
            
            objList.add(ar);
            resultByUserId.put(memberId, objList);
            System.debug('--- ar: ' + ar);
        }

        List<User> userUpdateList = new List<User>();

        for (Id userId : resultByUserId.keySet()) {
            List<AggregateResult> arList = resultByUserId.get(userId);

            Integer onTimeCount = 0;
            Integer lateCount = 0;

            for (AggregateResult ar : arList) {
                if ((Boolean)ar.get('submittedOnTime')) {
                    onTimeCount = (Integer) ar.get('cnt');
                    
                } else {
                    lateCount = (Integer) ar.get('cnt');
                }
            }

            System.debug('--- onTimeCount: ' + onTimeCount);
            System.debug('--- lateCount: ' + lateCount);

            Decimal totalCount = onTimeCount + lateCount;
            Decimal submissionRate = (onTimeCount / totalCount) * 100;

            System.debug('--- submissionRate: ' + submissionRate);

            userUpdateList.add( 
                new User(
                    Id = userId, 
                    On_Time_Timesheet_Submissions__c = submissionRate.setScale(2)
                )
            );
        }

        System.debug('--- userUpdateList : ' + userUpdateList);

        if (!userUpdateList.isEmpty()) {
            update userUpdateList;
        }
    }

    public void finish(Database.BatchableContext BC) {

    }

}