@isTest
private class CalculateOnTimeSubmissionBatchTest {
    @testSetup
    private static void testSetup() {
        //create team member
        List<Team_Member__c> teamMemberList = TestDataFactory.createTeamMembers(1, 'Astro');
        
        //create projects
        List<Project__c> projectList = TestDataFactory.createProjects(1, 'Project');

        //create project assignment
        List<Project_Assignment__c> projAssignments = TestDataFactory.createProjectAssignments(projectList, teamMemberList[0]);

        //create timesheet entries 
        TestDataFactory.createTimesheetEntry(projAssignments[0],14);
    }
    @isTest
    private static void calculationTest() {
        
        // Test data setup
        User currentUser = [SELECT 
            Id, On_Time_Timesheet_Submissions__c 
            FROM User 
            WHERE FirstName LIKE 'Astro%' AND LastName LIKE 'TestUser0%' LIMIT 1];
            
        List<Timesheet_Entry__c> entries = [
            SELECT Id , Timesheet_Date__c
            FROM Timesheet_Entry__c 
            WHERE Team_Member__r.User__r.Id = :currentUser.Id];
            

        Date lateDate = Date.today().addDays(-35);
        Date onTimeDate = Date.today().addDays(-12);
        List<Timesheet_Entry__c> entriesToSubmit = new List<Timesheet_Entry__c>();
        for (Integer i = 0; i < entries.size(); i++) {
            Timesheet_Entry__c entry = entries.get(i);
            if( i < entries.size()/2){
                entry.Timesheet_Date__c = lateDate.addDays(i);
            } else {
                entry.Timesheet_Date__c = onTimeDate.addDays(i);
            }
            entry.Locked__c = true;
            entriesToSubmit.add(entry);
        }
       
        update entriesToSubmit;
        // Actual test
        Test.startTest();
        CalculateOnTimeSubmissionBatch calculateBatchJob = new CalculateOnTimeSubmissionBatch();
        DataBase.executeBatch(calculateBatchJob); 
        Test.stopTest();

        // Asserts
        User currentUserUpdated = [SELECT 
            Id, On_Time_Timesheet_Submissions__c 
            FROM User 
            WHERE FirstName LIKE 'Astro%' AND LastName LIKE 'TestUser0%' LIMIT 1];
        
        System.assert(currentUserUpdated.On_Time_Timesheet_Submissions__c < 100.00);
    }

}