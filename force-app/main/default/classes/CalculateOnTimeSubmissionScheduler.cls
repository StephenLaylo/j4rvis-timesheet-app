global class CalculateOnTimeSubmissionScheduler implements Schedulable {
    public static String sched = '0 0 12 ? * 6 *'; //Every Friday at 12PM 

    global void execute(SchedulableContext SC) {
        CalculateOnTimeSubmissionBatch calculateBatch = new CalculateOnTimeSubmissionBatch(); 
        Database.executeBatch(calculateBatch,200);
    }
}