@isTest
private class CalculateOnTimeSubmissionSchedulerTest {

    private static String weeklySched = '0 0 12 ? * 6 *';

    @isTest
    private static void scheduleCalculateOnTimeSubmissionTest() {
        // Actual test
        Test.startTest();
        CalculateOnTimeSubmissionScheduler calculateScheduler = new CalculateOnTimeSubmissionScheduler();
        Id jobId = System.schedule('TestBatch', CalculateOnTimeSubmissionScheduler.sched, calculateScheduler);
        Test.stopTest();

        // Asserts
        CronTrigger ct = [Select id , CronExpression from CronTrigger where id = :jobId];
        System.assertEquals(ct.CronExpression, weeklySched);
    }

}