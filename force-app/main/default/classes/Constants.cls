public class Constants {

    //Logs
    public static final String LOG_LEVEL_ERROR = 'Error';
    public static final String LOG_LEVEL_DEBUG = 'Debug';
    public static final String LOG_LEVEL_INFO = 'Info';
    public static final String LOG_LEVEL_WARN = 'Warning';

}