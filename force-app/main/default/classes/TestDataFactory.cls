@isTest
public class TestDataFactory {

    public static Team_Member__c createTeamMember(User user) {
        Team_Member__c member = new Team_Member__c(
            Name = user.Name,
            User__c = user.Id,
            Annual_Salary__c = 100000
        );
        
        return member;
    }

    public static List<Team_Member__c> createTeamMembers(Integer recordCount, String firstName) {
        Profile stdProfile = [
            SELECT Id 
            FROM Profile 
            WHERE Name = 'Standard User'
        ]; 

        List<User> userList = new List<User>();

        for (Integer i = 0; i < recordCount; i ++) {
            String rand = '' + (Math.random() * 10);
            User u = new User(
                Alias = 'standt' + i, 
                Email = 'standarduser'+ i + '_' + rand + '@j4rvisTest.com',
                EmailEncodingKey = 'UTF-8', 
                FirstName = firstName + ' ' + rand, 
                LastName = 'TestUser' + i + '_' + rand, 
                LanguageLocaleKey = 'en_US', 
                LocaleSidKey = 'en_AU', 
                ProfileId = stdProfile.Id, 
                TimeZoneSidKey='Australia/Sydney', 
                UserName='standarduser'+ i + '_' + rand + '@j4rvisTest.com'
            );

            userList.add(u);
        }

        insert userList;

        List<Team_Member__c> teamMemberList = new List<Team_Member__c>();

        for (User u : userList) {
            Team_Member__c member = createTeamMember(u);
            teamMemberList.add(member);
        }

        insert teamMemberList;

        return teamMemberList;
    }

    public static Project__c createProject(String projName, Boolean isBillable) {
        //ToDo: record type? 
        Project__c project = new Project__c(
            Name = projName,
            Billable_Project__c	 = isBillable, 
            Sold_Budget__c = 500, 
            Sold_Margin__c = 50, 
            Project_Manager__c = createTeamMembers(1, 'Charlotte Katakuri')[0].Id
        );

        return project;
    }

    public static List<Project__c> createProjects(Integer recordCount, String recordTypeName) {
        Schema.DescribeSObjectResult objectResult = Schema.SObjectType.Project__c;
        Map<String,Schema.RecordTypeInfo> rtMapByName = objectResult.getRecordTypeInfosByName();
        Id rtId = rtMapByName != null && rtMapByName.containsKey(recordTypeName) ? rtMapByName.get(recordTypeName).getRecordTypeId() : null;

        List<Project__c> projectList = new List<Project__c>();

        for (Integer i = 0; i < recordCount; i ++) {
            Project__c proj = createProject('SampleProject' + i, true);
            proj.RecordTypeId = rtId;
            projectList.add(proj);
        }

        insert projectList;

        return projectList;
    }

    public static Project_Assignment__c createProjectAssignment(Project__c proj, Team_Member__c member) {
        Project_Assignment__c projectAssignment = new Project_Assignment__c(
            Project__c = proj.Id,
            Team_Member__c = member.Id, 
            Daily_Rate__c = 150, 
            Forecasted_Daily_Cost__c = 150
        );

        return projectAssignment;
    }

    public static List<Project_Assignment__c> createProjectAssignments(List<Project__c> projectList, Team_Member__c member) {
        List<Project_Assignment__c> paList = new List<Project_Assignment__c>();
        
        for (Project__c proj : projectList) {
            paList.add(createProjectAssignment(proj, member));
        }

        insert paList;

        return paList;
    }

    public static List<Timesheet_Entry__c> createTimesheetEntry(Project_Assignment__c projAssignment, Integer recordCount) {
        DateTime now = DateTime.now();
        Date today = Date.today();
        Date timesheetDate = (now.format('EEEE') == 'Sunday' ? today.addDays(-6) : today.toStartOfWeek().addDays(1));

        List<Timesheet_Entry__c> timesheetEntryList = new List<Timesheet_Entry__c>();

        for (Integer i = 0; i < recordCount; i ++) {
            Timesheet_Entry__c timeEntry = new Timesheet_Entry__c(
                Project_Assignment__c = projAssignment.Id, 
                Project__c = Id.valueOf(projAssignment.Project__c), 
                Team_Member__c = Id.valueOf(projAssignment.Team_Member__c), 
                Forecasted_Hours__c = 8, 
                Timesheet_Date__c = timesheetDate.addDays(i)
            );

            timesheetEntryList.add(timeEntry);
        }

        insert timesheetEntryList;
        
        return timesheetEntryList;
    }
    
}