/**
 * @author: Jannis Bott
 * @company J4RVIS 
 * @description: Service that saves and returns timesheet data used in the timesheeting application.
**/
public without sharing class TimesheetService {

    /**
    * @description Method to retrieve map of project assignments by project name
    * @author Stephen Laylo, Jen Tan
    * @company J4RVIS 
    * @param userId team member's user Id
    * @param startDate date of the timesheet entry
    * @param timeframe
    * @param numOfDaysToShow number of days the timeframe will display
    * @return Map<String, List<Project_Assignment__c>> 
    **/
    @AuraEnabled(cacheable=false)
    public static Map<String, List<Project_Assignment__c>> getProjectRecordTypes(Id userId, Datetime startDate, String timeframe, Integer numOfDaysToShow) {
        Date localStartDate = Datetime.newInstance(startDate.getTime()).date();
        Date localEndDate;
        
        if (timeframe == 'week') {
            localStartDate = getLocalStartDate(startDate);
            localEndDate = localStartDate.addDays(numOfDaysToShow - 1);
 
        } else {
            localEndDate = localStartDate.addDays(6);
        }
        
        List<Project_Assignment__c> projectAssignments = [
            SELECT Id, 
                   Project__r.Name, 
                   Project__r.RecordType.Name, 
                   Project_Role__r.Name, 
                   Team_Member__c, 
                   (
                       SELECT Id,       
                              Project__r.RecordType.Name, 
                              Locked__c, 
                              Project_Assignment__c,
                              Project_Assignment__r.Project__r.Name,      
                              Project_Assignment__r.Completed__c,
                              Project_Assignment__r.Project_Role__r.Name,
                              Project__c, 
                              Timesheet_Date__c, 
                              Forecasted_Hours__c,
                              Hours__c, 
                              Actual_Non_Billable_Hours__c, 
                              Work_Summary__c 
                       FROM Timesheet_Entries__r
                       WHERE Timesheet_Date__c >= :localStartDate AND 
                             Timesheet_Date__c <= :localEndDate
                       ORDER BY Timesheet_Date__c ASC
                   )
            FROM Project_Assignment__c
            WHERE Team_Member__r.User__c = :userId AND 
                  Completed__c = false
            ORDER BY Project__r.RecordType.Name DESC, Project__r.Name DESC
        ];

        Map<String, List<Project_Assignment__c>> projectAssignmentsByProjectRecordTypes = new Map<String, List<Project_Assignment__c>>();
 
        for (Project_Assignment__c projectAssignment : projectAssignments) {
            if (projectAssignmentsByProjectRecordTypes.containsKey(projectAssignment.Project__r.RecordType.Name)) {
                projectAssignmentsByProjectRecordTypes.get(projectAssignment.Project__r.RecordType.Name).add(projectAssignment);

            } else {
                projectAssignmentsByProjectRecordTypes.put(projectAssignment.Project__r.RecordType.Name, new List<Project_Assignment__c> { projectAssignment });
            }
        }
        
        return projectAssignmentsByProjectRecordTypes;
    }

    /**
    * @description Method to upsert timesheet entries
    * @author Stephen Laylo, Jen Tan
    * @company J4RVIS 
    * @param entries list of timesheet entries to upsert
    * @return List<Timesheet_Entry__c> 
    **/
    @AuraEnabled
    public static List<Timesheet_Entry__c> upsertTimesheetEntries(List<Timesheet_Entry__c> entries) {
        System.debug(entries);

        try {
            upsert entries;

            return entries;

        } catch (Exception ex) {
            System.debug(ex);

            LogUtil.getInstance().addLog(
                'TimesheetService',
                'upsertTimesheetEntries',
                ex,
                ex.getMessage(),
                '',
                'Trying to upsert multiple Timesheet_Entry__c records.' + '\n' +
                'entries = ' + JSON.serializePretty(entries),
                '',
                ex.getStackTraceString(),
                '',
                Constants.LOG_LEVEL_ERROR
            ).createLogs(true);     

            return null;
        }
    }

    /**
    * @description Method to retrieve the sum of the forecasted and actual hours of the timesheet entries
    * @author Stephen Laylo
    * @company J4RVIS 
    * @param userId team member's user Id
    * @param startDate date of the timesheet entry
    * @param timeframe 
    * @param recordType Project record type name
    * @param numOfDaysToShow number of days the timeframe will display
    * @return List<AggregateResult> 
    **/
    @AuraEnabled(cacheable=true)
    public static List<AggregateResult> getTimesheetEntriesSummaryTotals(Id userId, Datetime startDate, String timeframe, String recordType, Integer numOfDaysToShow) {
        Date localStartDate = Datetime.newInstance(startDate.getTime()).date();
        Date localEndDate;

        if (timeframe == 'week') {
            localStartDate = getLocalStartDate(startDate);
            localEndDate = localStartDate.addDays(numOfDaysToShow - 1);

        } else {
            localEndDate = localStartDate.addDays(6);
        }

        List<AggregateResult> timesheetEntries;
        String queryString = 
            'SELECT SUM(Forecasted_Hours__c) forecastedHoursSum, ' +  
                   'SUM(Hours__c) hoursSum ' + 
            'FROM Timesheet_Entry__c ' + 
            'WHERE Project_Assignment__r.Team_Member__r.User__c = :userId AND ' + 
                  'Project_Assignment__r.Completed__c = false AND ' + 
                  'Timesheet_Date__c >= :localStartDate AND ' + 
                  'Timesheet_Date__c <= :localEndDate ';
        
        if (!String.isBlank(recordType)) {
            queryString += 'AND Project_Assignment__r.Project__r.RecordType.Name = :recordType ';
        }

        timesheetEntries = Database.query(queryString);
        
        return timesheetEntries;
    }

    /**
    * @description Method to retrieve the sum of the forecasted, actual, and non billable hours of the timesheet entries
    * @author Bea Rillorta
    * @company J4RVIS 
    * @param userId team member's user Id
    * @param startDate date of the timesheet entry
    * @param timeframe
    * @param numOfDaysToShow number of days the timeframe will display
    * @return List<AggregateResult> 
    **/
    @AuraEnabled(cacheable=true)
    public static List<AggregateResult> getTimesheetEntriesSummary(Id userId, Datetime startDate, String timeframe, Integer numOfDaysToShow) {
        Date localStartDate = Datetime.newInstance(startDate.getTime()).date();
        Date localEndDate;

        if (timeframe == 'week') {
            localStartDate = getLocalStartDate(startDate);
            localEndDate = localStartDate.addDays(numOfDaysToShow - 1);

        } else {
            localEndDate = localStartDate.addDays(6);
        }

        List<AggregateResult> timesheetEntries = [
            SELECT Timesheet_Date__c, 
                   SUM(Forecasted_Hours__c) forecastedHoursSum, 
                   SUM(Hours__c) hoursSum, 
                   SUM(Actual_Non_Billable_Hours__c) actualNonBillableSum
            FROM Timesheet_Entry__c
            WHERE Project_Assignment__r.Team_Member__r.User__c = :userId AND 
                  Project_Assignment__r.Completed__c = false AND 
                  Timesheet_Date__c >= :localStartDate AND 
                  Timesheet_Date__c <= :localEndDate
            GROUP BY Timesheet_Date__c
            ORDER BY Timesheet_Date__c ASC
        ];
        
        return timesheetEntries;
    }
    
    /**
    * @description Retrieve the list of holidays
    * @author Bea Rillorta
    * @company J4RVIS
    * @return List<Holiday> List of Holiday dates within the timeframe
    **/
    @AuraEnabled(cacheable=true)
    public static List<Holiday> getHolidays(Datetime startDate, String timeframe, Integer numOfDaysToShow) {
        Date localStartDate = Datetime.newInstance(startDate.getTime()).date();
        Date localEndDate;

        if (timeframe == 'week') {
            localStartDate = getLocalStartDate(startDate);
            localEndDate = localStartDate.addDays(numOfDaysToShow - 1);

        } else {
            localEndDate = localStartDate.addDays(6);
        }
        
        List<Holiday> holidayList = [
            SELECT Id, 
                   ActivityDate, 
                   Name
            FROM Holiday
            WHERE ActivityDate >= :localStartDate AND
                  ActivityDate <= :localEndDate
        ];

        return holidayList;
    }

    /**
    * @description Retrieve the list of holidays
    * @author Bea Rillorta
    * @company J4RVIS
    * @param startDate Date where the start of the week will be calculated
    * @return Date start of week based on the date provided
    **/
    private static Date getLocalStartDate(Datetime startDate) {
        Date localStartDate = Datetime.newInstance(startDate.getTime()).date();
        localStartDate = (startDate.format('EEEE') == 'Sunday' ? localStartDate.addDays(-6) : localStartDate.toStartOfWeek().addDays(1));

        return localStartDate;
    }
    
}