/**
 * @description       : 
 * @author            : Bea Rillorta
 * @group             : 
 * @last modified on  : 05-28-2021
 * @last modified by  : Bea Rillorta
 * Modifications Log 
 * Ver   Date         Author         Modification
 * 1.0   05-28-2021   Bea Rillorta   Initial Version
**/
@isTest
private with sharing class TimesheetServiceTest {
   
    @testSetup
    private static void testSetup() {
        //create team member
        List<Team_Member__c> teamMemberList = TestDataFactory.createTeamMembers(1, 'Astro');
        
        //create projects
        List<Project__c> projectList = TestDataFactory.createProjects(2, 'Project');
        List<Project__c> internalProjectList = TestDataFactory.createProjects(1, 'Internal'); 
        List<Project__c> preSalesProjectList = TestDataFactory.createProjects(1, 'Pre-Sales'); 
        List<Project__c> allProjects = new List<Project__c>();
        allProjects.addAll(projectList);
        allProjects.addAll(internalProjectList);
        allProjects.addAll(preSalesProjectList);

        //create project assignment
        List<Project_Assignment__c> projAssignments = TestDataFactory.createProjectAssignments(allProjects, teamMemberList[0]);

        //create timesheet entries 
        for (Project_Assignment__c pa : projAssignments) {
            TestDataFactory.createTimesheetEntry(pa,14);
        }
    }

    @isTest
    private static void getProjectRecordTypesTest() {
        User currentUser = [
            SELECT Id 
            FROM User 
            WHERE FirstName LIKE 'Astro%' AND 
                  LastName LIKE 'TestUser0%' 
            LIMIT 1
        ];

        Set<String> projRecTypeList = new Set<String>();
        Set<String> projRecTypeDefaultList = new Set<String>();
        
        Test.startTest();

        System.runAs(currentUser) {
            projRecTypeList = TimesheetService.getProjectRecordTypes(UserInfo.getUserId(),System.now(), 'week', 5).keySet();
            projRecTypeDefaultList = TimesheetService.getProjectRecordTypes(UserInfo.getUserId(),System.now(), null, 5).keySet();

        }

        Test.stopTest();

        System.assertEquals(3, projRecTypeList.size());
        System.assert(projRecTypeList.contains('Internal'));
        System.assert(projRecTypeList.contains('Pre-Sales'));
        System.assert(projRecTypeList.contains('Project'));
        System.assertEquals(3, projRecTypeDefaultList.size());
    }
  
    @isTest
    private static void getTimesheetEntriesSummaryTotalsTest() {
        User currentUser = [
            SELECT Id 
            FROM User 
            WHERE FirstName LIKE 'Astro%' AND 
                  LastName LIKE 'TestUser0%' 
            LIMIT 1
        ];
        
        List<AggregateResult> internalSummaryList = new List<AggregateResult>();
        List<AggregateResult> projectSummaryList = new List<AggregateResult>();
        List<AggregateResult> preSalesSummaryList = new List<AggregateResult>();
        List<AggregateResult> defaultSummaryList = new List<AggregateResult>();

        Test.startTest();

        System.runAs(currentUser) {
            internalSummaryList = TimesheetService.getTimesheetEntriesSummaryTotals(UserInfo.getUserId(), System.now(), null, 'Internal', 5);
            projectSummaryList = TimesheetService.getTimesheetEntriesSummaryTotals(UserInfo.getUserId(), System.now(), 'week', 'Project', 5);
            preSalesSummaryList = TimesheetService.getTimesheetEntriesSummaryTotals(UserInfo.getUserId(), System.now(), 'week', 'Pre-Sales', 7);
            defaultSummaryList = TimesheetService.getTimesheetEntriesSummaryTotals(UserInfo.getUserId(), System.now(), 'week', null, 5);
        }

        Test.stopTest();

        System.assertEquals(80, projectSummaryList[0].get('forecastedHoursSum'));
        System.assertEquals(null, projectSummaryList[0].get('hoursSum'));

        System.assertEquals(56, internalSummaryList[0].get('forecastedHoursSum'));
        System.assertEquals(null, internalSummaryList[0].get('hoursSum'));

        System.assertEquals(56, preSalesSummaryList[0].get('forecastedHoursSum'));
        System.assertEquals(null, preSalesSummaryList[0].get('hoursSum'));

        System.assertEquals(160, defaultSummaryList[0].get('forecastedHoursSum'));
        System.assertEquals(null, defaultSummaryList[0].get('hoursSum'));
    }

    @isTest
    private static void upsertTimesheetEntriesTest() {
        User currentUser = [
            SELECT Id 
            FROM User 
            WHERE FirstName LIKE 'Astro%' AND 
                  LastName LIKE 'TestUser0%' 
            LIMIT 1
        ];

        List<Timesheet_Entry__c> timeEntries = [
            SELECT Id 
            FROM Timesheet_Entry__c 
            WHERE Team_Member__r.User__c = :currentUser.Id
        ];
        
        Test.startTest();

        System.runAs(currentUser) {
           for (Timesheet_Entry__c te : timeEntries) {
               te.Hours__c = 8;
           }

           TimesheetService.upsertTimesheetEntries(timeEntries);
        }
    
        Test.stopTest();

        List<Timesheet_Entry__c> timeEntriesUpdated = [
            SELECT Id, Hours__c 
            FROM Timesheet_Entry__c 
            WHERE Team_Member__r.User__c = :currentUser.Id
        ];

        for (Timesheet_Entry__c teUpdated : timeEntriesUpdated) {
            System.assertEquals(8, teUpdated.Hours__c);
        }
    }

    @isTest
    private static void getTimesheetEntriesSummaryTest() {
        User currentUser = [
            SELECT Id 
            FROM User 
            WHERE FirstName LIKE 'Astro%' AND 
                  LastName LIKE 'TestUser0%' 
            LIMIT 1
        ];
        
        List<AggregateResult> teSummaryList = new List<AggregateResult>();
        List<AggregateResult> teSummaryDefaultList = new List<AggregateResult>();

        Test.startTest();

        System.runAs(currentUser) {
            teSummaryList = TimesheetService.getTimesheetEntriesSummary(UserInfo.getUserId(), System.now(), 'week', 5);
            teSummaryDefaultList = TimesheetService.getTimesheetEntriesSummary(UserInfo.getUserId(), System.now(), null, 5);
        }

        Test.stopTest();
        
        for (AggregateResult ar : teSummaryList) {
            System.assertEquals(32, ar.get('forecastedHoursSum'));
            System.assertEquals(null, ar.get('hoursSum'));
            System.assertEquals(null, ar.get('actualNonBillableSum'));
        }

        for (AggregateResult ar : teSummaryDefaultList) {
            System.assertEquals(32, ar.get('forecastedHoursSum'));
            System.assertEquals(null, ar.get('hoursSum'));
            System.assertEquals(null, ar.get('actualNonBillableSum'));
        }
    }

    @isTest
    private static void getHolidaysTest() {
        Holiday testHoliday = new Holiday();

        testHoliday.Name = 'Test Holiday';
        testHoliday.Activitydate = System.Today();
        
        insert testHoliday;

        User currentUser = [
            SELECT Id 
            FROM User 
            WHERE FirstName LIKE 'Astro%' AND 
                  LastName LIKE 'TestUser0%' 
            LIMIT 1
        ];

        List<Holiday> holidayList = new List<Holiday>();
        List<Holiday> holidayListDefault = new List<Holiday>();

        Test.startTest();

        System.runAs(currentUser) {
            holidayList = TimesheetService.getHolidays(System.now(), 'week', 5);
            holidayListDefault = TimesheetService.getHolidays(System.now(), null, 7);
        }

        Test.stopTest();

        System.assert(!holidayList.isEmpty());
        System.assert(!holidayListDefault.isEmpty());
    }
}