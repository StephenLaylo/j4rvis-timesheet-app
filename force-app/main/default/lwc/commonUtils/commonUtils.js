const TWENTY_FOUR_HOURS = 24;
const SIXTY_MINUTES = 60;
const SIXTY_SECONDS = 60;
const ONE_THOUSAND_MILLISECONDS = 1000;
const ONE_DAY_IN_MILLISECONDS = TWENTY_FOUR_HOURS * SIXTY_MINUTES * SIXTY_SECONDS * ONE_THOUSAND_MILLISECONDS;
const DAYS_LIST = ['Sunday', 'Monday', 'Tuesday', 'Wednesday', 'Thursday', 'Friday', 'Saturday'];

const arrayGroupBy = (array, keys) => {
    let grouped = {};

    array.forEach(function (a) {
        keys.reduce(function (o, g, i) {
            o[a[g]] = o[a[g]] || (i + 1 === keys.length ? [] : {});
            return o[a[g]];
        }, grouped).push(a);
    });

    return grouped;
}

const arrayDistinctTrim = (array) => {
    return array.filter((value, index, self) => self.indexOf(value) === index);
}

const startOfWeek = (date) => {
    let d = new Date(date.getFullYear(), date.getMonth(), date.getDate());
    
    let diff = d.getDate() - ((d.getDay() == 0 ? 7 : d.getDay()) - 1);

    return new Date(d.setDate(diff));  
}

const endOfWeek = (date, numOfDaysToShow) => {
    let lastday = startOfWeek(date).getTime() + (ONE_DAY_IN_MILLISECONDS * (numOfDaysToShow - 1));

    return new Date(lastday);
}

const getCurrentWeekArrayOfDates = (date, numOfDaysToShow) => {
    let dates = [];

    for (let i = 0; i < numOfDaysToShow; i ++) {
        let diff = startOfWeek(date).getTime() + (ONE_DAY_IN_MILLISECONDS * i);
        let d = new Date(diff);

        dates.push({
            formattedTimesheetDate: d,
            dayName: DAYS_LIST[d.getDay()], 
            isDummy: true
        });
    }

    return dates;
}

const getCurrentWeekTimesheetDates = (date, numOfDaysToShow, timesheetEntries) => {
    let dummyDatesOfTheWeek = getCurrentWeekArrayOfDates(date, numOfDaysToShow);
    let validatedDatesOfTheWeeks = [];

    dummyDatesOfTheWeek.forEach(dummyDateOfTheWeek => {
        let actualTimesheetEntryValue = timesheetEntries.find(timesheetEntry => dummyDateOfTheWeek.formattedTimesheetDate.getTime() == timesheetEntry.formattedTimesheetDate.getTime() && dummyDateOfTheWeek.dayName == timesheetEntry.dayName);

        if (actualTimesheetEntryValue != null) {
            actualTimesheetEntryValue.isDummy = false;
            validatedDatesOfTheWeeks.push(actualTimesheetEntryValue);

        } else {
            validatedDatesOfTheWeeks.push(dummyDateOfTheWeek);
        }
    });

    return validatedDatesOfTheWeeks;
}

export {
    arrayGroupBy, 
    arrayDistinctTrim, 
    startOfWeek, 
    endOfWeek, 
    getCurrentWeekArrayOfDates, 
    getCurrentWeekTimesheetDates, 
    ONE_DAY_IN_MILLISECONDS, 
    DAYS_LIST
}