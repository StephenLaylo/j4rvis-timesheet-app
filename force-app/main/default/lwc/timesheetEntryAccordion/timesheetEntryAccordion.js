import { LightningElement, api, wire } from 'lwc';
import { fireEvent, registerListener, unregisterAllListeners } from "c/pubsub";
import { CurrentPageReference } from 'lightning/navigation';

export default class TimesheetEntryAccordion extends LightningElement {

    @api selectedDate;
	@api numOfDaysToShow;

    projectTypesWithProjectAssignments;

	activeSections = [
		'Project'
	];

	@api 
	set projectTypesWithProjectAssignmentsRef(value) {
		this.projectTypesWithProjectAssignments = null;
		this.projectTypesWithProjectAssignments = JSON.parse(JSON.stringify(value));
	}

	get projectTypesWithProjectAssignmentsRef() {
		return this.projectTypesWithProjectAssignments;
	}

	@wire(CurrentPageReference) 
	pageRef;

	connectedCallback() {
		registerListener("triggerSummaryTotalsCalculation", this.triggerSummaryTotalsCalculation, this);
	}

	disconnectedCallback() {
		unregisterAllListeners(this);
	}

	handleTimesheetEntryHoursOnBlur(event) {
		this.updateArrayRealtime(event);
    }

    handleTimesheetEntryNonBillableHoursOnBlur(event) {
		this.updateArrayRealtime(event);
	}

    handleTimesheetEntryCommentsOnBlur(event) {
		this.updateArrayRealtime(event);
    }

	updateArrayRealtime(event) {
		let projectAssignments = event.detail.projectAssignments;

		if (projectAssignments) {
			const evtCustomEvent = new CustomEvent('updatearrayrealtime', {   
				detail: {
					projectAssignments: projectAssignments
				}
			});

			this.dispatchEvent(evtCustomEvent);
		}
	}

	@api
	getProjectSelectorComponents() {
		return this.template.querySelectorAll('c-timesheet-entry-project-selector');
	}

	triggerSummaryTotalsCalculation() {
		let projectAssignments = [];

		let projectSelectorComponents = this.getProjectSelectorComponents();
		if (projectSelectorComponents) {
			projectSelectorComponents.forEach(projectSelectorComponent => {
				let projectAssignment = projectSelectorComponent.getProjectAssignments();
				if (projectAssignment) {
					projectAssignment.forEach(element => {
						projectAssignments.push(element);
					});
				}
			});
		}

		fireEvent(this.pageRef, "refreshTimesheetEntriesSummaryTotalsTemp", projectAssignments);
	}

}