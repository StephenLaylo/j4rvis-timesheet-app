import { LightningElement, wire, api } from 'lwc';
import userId from '@salesforce/user/Id';
import { getRecord, getFieldValue } from 'lightning/uiRecordApi';
import { subscribe, MessageContext } from 'lightning/messageService';
import TIMESHEET_ENTRY_HEADER_CHANNEL from '@salesforce/messageChannel/Timesheet_Entry_Header_Update__c';
import { startOfWeek, endOfWeek } from "c/commonUtils";

import NAME_FIELD from '@salesforce/schema/User.Name';
import UTILISATION_FIELD from '@salesforce/schema/User.Utilisation__c';
import SUBMISSION_FIELD from '@salesforce/schema/User.On_Time_Timesheet_Submissions__c';
import PHOTO_FIELD from '@salesforce/schema/User.SmallPhotoUrl';

export default class TimesheetEntryHeader extends LightningElement {

    //helper variables
    userId = userId; 
    heading = 'slds-text-heading_medium ';
    weekStart;
    weekEnd;
    timesheetEntryHeaderSelectedDate;
    timesheetEntryHeaderNumberOfDaysToShow = 5;

    //configurable properties
    @api utilisationTarget = 0.75;
    @api submissionTarget = 0.99;
    
    @wire(MessageContext)
    messageContext;

    //user wire service
    @wire(getRecord, { 
        recordId: '$userId', 
        fields: [
            NAME_FIELD, 
            UTILISATION_FIELD, 
            SUBMISSION_FIELD, 
            PHOTO_FIELD
        ] 
    }) 
    user;

    subscribeToMessageChannel() {
        this.subscription = subscribe(
            this.messageContext,
            TIMESHEET_ENTRY_HEADER_CHANNEL,
            (message) => this.handleMessageTimesheetEntryHeader(message)
        );
    }

    connectedCallback() {
        this.timesheetEntryHeaderSelectedDate = new Date();
        this.weekStart = startOfWeek(this.timesheetEntryHeaderSelectedDate);
        this.weekEnd = endOfWeek(this.timesheetEntryHeaderSelectedDate, this.timesheetEntryHeaderNumberOfDaysToShow);

        this.subscribeToMessageChannel();
    }

    handleMessageTimesheetEntryHeader(message) {
        if (message != null) {
            this.updateCurrentDate(message);
        }
    }

    updateCurrentDate(message) {
        this.timesheetEntryHeaderSelectedDate = new Date(message.selectedDate);
        this.timesheetEntryHeaderNumberOfDaysToShow = message.numOfDaysToShow;
        this.weekStart = startOfWeek(this.timesheetEntryHeaderSelectedDate);
        this.weekEnd = endOfWeek(this.timesheetEntryHeaderSelectedDate, message.numOfDaysToShow);
    }

    get name() {
        return getFieldValue(this.user.data, NAME_FIELD);
    }

    get utilisation() {
        return getFieldValue(this.user.data, UTILISATION_FIELD) / 100;
    }

    get submissions() {
        return getFieldValue(this.user.data, SUBMISSION_FIELD) / 100;
    }
    
    get photo() {
        return getFieldValue(this.user.data, PHOTO_FIELD);
    }
    
    //sets the color to green/red depending on attainment of target
    get utilisationCss() {
        return this.utilisation > this.utilisationTarget ? this.heading + 'positive' : this.heading + 'negative';
    }

    //sets the color to green/red depending on attainment of target
    get submissionsCss() {
        return this.submissions > this.submissionTarget ? this.heading + 'positive' : this.heading + 'negative';
    }

}