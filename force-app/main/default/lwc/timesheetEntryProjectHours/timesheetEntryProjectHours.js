import { api, track, LightningElement } from 'lwc';

export default class TimesheetEntryProjectHours extends LightningElement {

    @api timesheetEntry;
    @api hideNonBillableEntry = false;
    @api hideComments = false;
    @api projectAssignmentId;

    @track maxHours;

    timesheetEntryHoursOnBlur(event) {
        const evtCustomEvent = new CustomEvent('timesheetentryhoursblur', {   
            detail: {
                projectAssignmentId: this.projectAssignmentId, 
                timesheetEntryId: this.timesheetEntry.Id, 
                timesheetEntryHour: (event.target.value != null && event.target.value != '' && Number(event.target.value) > 0 ? Number(event.target.value) : null)
            }
        });

        this.dispatchEvent(evtCustomEvent);
    }

    timesheetEntryNonBillableHoursOnBlur(event) {
        const evtCustomEvent = new CustomEvent('timesheetentrynonbillablehoursblur', {   
            detail: {
                projectAssignmentId: this.projectAssignmentId, 
                timesheetEntryId: this.timesheetEntry.Id, 
                timesheetEntryNonBillableHour: (event.target.value != null && event.target.value != '' && Number(event.target.value) > 0 ? Number(event.target.value) : null)
            }
        });

        this.dispatchEvent(evtCustomEvent);
    }

    timesheetEntryCommentsOnBlur(event) {
        const evtCustomEvent = new CustomEvent('timesheetentrycommentsblur', {   
            detail: {
                projectAssignmentId: this.projectAssignmentId, 
                timesheetEntryId: this.timesheetEntry.Id, 
                timesheetEntryComment: event.target.value
            }
        });

        this.dispatchEvent(evtCustomEvent);
    }

    @api
    validateInput() {
        let valid = true;

        if (!this.timesheetEntry.Locked__c) {        
            let commentField = this.template.querySelector('lightning-textarea[title="Comments"]');
            
            if (commentField) {
                if (this.timesheetEntry.Hours__c) {
                    commentField.required = true;

                } else {
                    commentField.required = false;
                }

                valid = commentField.checkValidity();
                commentField.reportValidity();
            }
        }

        return valid;
    }

    @api
    forecastedHoursMet() {
        let forecastedHoursMet = true;

        if (this.timesheetEntry.Forecasted_Hours__c) {
            if (!this.timesheetEntry.Hours__c || (this.timesheetEntry.Hours__c < this.timesheetEntry.Forecasted_Hours__c)) {
                forecastedHoursMet = false;
            }
        }

        return forecastedHoursMet;
    }

    get isTimesheetEnabled() {
        return this.timesheetEntry.Id;
    }
    
}