import { api, wire, LightningElement } from 'lwc';
import { CurrentPageReference } from 'lightning/navigation';
import { fireEvent } from "c/pubsub";
import { startOfWeek, ONE_DAY_IN_MILLISECONDS } from "c/commonUtils";

export default class TimesheetEntryProjectSelector extends LightningElement {

	@api projectRecordType;
	@api projectAssignmentsRef;
	@api selectedDate;
	@api numOfDaysToShow;

	projectAssignments;
	hideNonBillableEntry = false;
	hideComments = false;

	// loading = true;

	@wire(CurrentPageReference) 
	pageRef;

	connectedCallback() {
		this.initializeTimeEntryDisplay();
	}
	
	@api
	initializeTimeEntryDisplay() {
		if (this.projectAssignmentsRef) {
			this.projectAssignments = JSON.parse(JSON.stringify(this.projectAssignmentsRef));

			if (this.projectAssignments != null) {
				this.projectAssignments.forEach(projectAssignment => {
					if (projectAssignment.Timesheet_Entries__r != null) {
						projectAssignment.Timesheet_Entries__r.forEach(timesheetEntry => {
							let dateToday = new Date();
							let dateTodayNoTimestamp = new Date(dateToday.getFullYear(), dateToday.getMonth(), dateToday.getDate());

							let timesheetEntryDate = new Date(timesheetEntry.Timesheet_Date__c);
							let timesheetEntryDateNoTimestamp = new Date(timesheetEntryDate.getFullYear(), timesheetEntryDate.getMonth(), timesheetEntryDate.getDate());
							
							let saturdays = new Date(startOfWeek(timesheetEntryDate).getTime() + (ONE_DAY_IN_MILLISECONDS * 5));
							let saturdaysNoTimestamp = new Date(saturdays.getFullYear(), saturdays.getMonth(), saturdays.getDate());
							
							let sundays = new Date(startOfWeek(timesheetEntryDate).getTime() + (ONE_DAY_IN_MILLISECONDS * 6));
							let sundaysNoTimestamp = new Date(sundays.getFullYear(), sundays.getMonth(), sundays.getDate());

							timesheetEntry.cssClass = 'slds-p-around_x-small';
							
							if (timesheetEntryDateNoTimestamp.getTime() == dateTodayNoTimestamp.getTime()) {
								timesheetEntry.cssClass += ' timesheet-today';

							} else if (timesheetEntry.isHoliday === true) {
								timesheetEntry.cssClass += ' timesheet-holiday';

							} else if (timesheetEntryDateNoTimestamp.getTime() == saturdaysNoTimestamp.getTime() || timesheetEntryDateNoTimestamp.getTime() == sundaysNoTimestamp.getTime()) {
								timesheetEntry.cssClass += ' timesheet-weekend';
							}

							if (timesheetEntry.isDummy) {
								timesheetEntry.cssClass += ' slds-theme_shade';
							}
							
						});
					}
				});
			}
		}

		if (this.projectRecordType == 'Internal') {
			this.hideNonBillableEntry = true;
			this.hideComments = true;
			
		} else if (this.projectRecordType == 'Pre-Sales') {
			this.hideNonBillableEntry = true;
		}
	}

	@api
	getProjectAssignments() {
		return this.projectAssignments;
	}

	@api
	validateInput() {
		let valid = true;
		let timesheetEntryProjectHoursComponent = this.template.querySelectorAll('c-timesheet-entry-project-hours');

		if (timesheetEntryProjectHoursComponent) {
            timesheetEntryProjectHoursComponent.forEach(element => {
				let isInputValid = element.validateInput();

				valid = valid && isInputValid;                
            });
        }

		return valid;
	}

	@api
	validateIfForecastedHoursMet() {
		let forecastedHoursMet = true;
		let timesheetEntryProjectHoursComponent = this.template.querySelectorAll('c-timesheet-entry-project-hours');

		if (timesheetEntryProjectHoursComponent) {
            timesheetEntryProjectHoursComponent.forEach(element => {
				let isForecastedHoursMet = element.forecastedHoursMet();
				forecastedHoursMet = forecastedHoursMet && isForecastedHoursMet;
            });
        }

		return forecastedHoursMet;
	}

	handleTimesheetEntryHoursOnBlur(event) {
		let timesheetEntries = this.projectAssignments.find(projectAssignment => (projectAssignment.Id == event.detail.projectAssignmentId)).Timesheet_Entries__r;
		let timesheetEntry = timesheetEntries.find(timesheetEntry => (timesheetEntry != null && timesheetEntry.Id == event.detail.timesheetEntryId));

		timesheetEntry.Hours__c = event.detail.timesheetEntryHour;

		const evtCustomEvent = new CustomEvent('timesheetentryhoursblur', {   
            detail: {
                projectAssignments: this.projectAssignments
            }
        });

        this.dispatchEvent(evtCustomEvent);
		this.passProjectAssignmentsArray(this.projectAssignments);
    }

    handleTimesheetEntryNonBillableHoursOnBlur(event) {
		let timesheetEntries = this.projectAssignments.find(projectAssignment => (projectAssignment.Id == event.detail.projectAssignmentId)).Timesheet_Entries__r;
		let timesheetEntry = timesheetEntries.find(timesheetEntry => (timesheetEntry != null && timesheetEntry.Id == event.detail.timesheetEntryId));

        timesheetEntry.Actual_Non_Billable_Hours__c = event.detail.timesheetEntryNonBillableHour;

		const evtCustomEvent = new CustomEvent('timesheetentrynonbillablehoursblur', {   
            detail: {
                projectAssignments: this.projectAssignments
            }
        });

        this.dispatchEvent(evtCustomEvent);
		this.passProjectAssignmentsArray(this.projectAssignments);
	}

    handleTimesheetEntryCommentsOnBlur(event) {
		let timesheetEntries = this.projectAssignments.find(projectAssignment => (projectAssignment.Id == event.detail.projectAssignmentId)).Timesheet_Entries__r;
		let timesheetEntry = timesheetEntries.find(timesheetEntry => (timesheetEntry != null && timesheetEntry.Id == event.detail.timesheetEntryId));

        timesheetEntry.Work_Summary__c = event.detail.timesheetEntryComment;

		const evtCustomEvent = new CustomEvent('timesheetentrycommentsblur', {   
            detail: {
                projectAssignments: this.projectAssignments
            }
        });

        this.dispatchEvent(evtCustomEvent);
    }

	passProjectAssignmentsArray(projectAssignments) {
		fireEvent(this.pageRef, "refreshTimesheetEntriesSummaryTotalsTemp" + this.projectRecordType, projectAssignments);
		fireEvent(this.pageRef, "triggerSummaryTotalsCalculation", null);
	}

	get numOfDaysColumns() {
		return (this.numOfDaysToShow == 5 ? 'slds-size_1-of-5' : 'slds-size_1-of-7')
	}

}