import { LightningElement, wire } from 'lwc';
import userId from "@salesforce/user/Id";
import { startOfWeek, getCurrentWeekTimesheetDates, ONE_DAY_IN_MILLISECONDS, DAYS_LIST } from "c/commonUtils";
import getProjectRecordTypes from "@salesforce/apex/TimesheetService.getProjectRecordTypes";
import getHolidays from "@salesforce/apex/TimesheetService.getHolidays";
import upsertTimesheetEntries from '@salesforce/apex/TimesheetService.upsertTimesheetEntries';
import { publish, subscribe, MessageContext } from 'lightning/messageService';
import TIMESHEET_ENTRY_WRAPPER_CHANNEL from '@salesforce/messageChannel/Timesheet_Entry_Wrapper_Update__c';
import TIMESHEET_ENTRY_TOTAL_HOURS_WRAPPER_CHANNEL from '@salesforce/messageChannel/Timesheet_Entry_Total_Hours_Wrapper_Update__c';
import TIMESHEET_ENTRY_TOTAL_HOURS_SUMMARY_CHANNEL from '@salesforce/messageChannel/Timesheet_Entry_Total_Hours_Summary_Update__c';
import TIMESHEET_ENTRY_HEADER_CHANNEL from '@salesforce/messageChannel/Timesheet_Entry_Header_Update__c';
import { CurrentPageReference } from 'lightning/navigation';
import { fireEvent } from "c/pubsub";
import { ShowToastEvent } from 'lightning/platformShowToastEvent';

const SEVEN_DAYS_IN_MILLISECONDS = 7 * ONE_DAY_IN_MILLISECONDS;

export default class TimesheetEntryWrapper extends LightningElement {

	currentUserId = userId;
    selectedDate = new Date();
	numOfDaysToShow = 5;
	selectedTimePeriod = "week";
    timesheetEntry;
	projectTypesWithProjectAssignments;

	loading = true;

	showSubmissionConfirmationDialog = false;

	@wire(MessageContext)
    messageContext;

	@wire(CurrentPageReference) 
	pageRef;

	refreshData(callback) {
		getHolidays({
			startDate: this.selectedDate,
			timeframe: this.selectedTimePeriod,
			numOfDaysToShow: this.numOfDaysToShow

		}).then(holidays => {
			getProjectRecordTypes({
				userId: this.currentUserId,
				startDate: this.selectedDate,
				timeframe: this.selectedTimePeriod,
				numOfDaysToShow: this.numOfDaysToShow
	
			}).then(projectTypesWithProjectAssignments => {
				this.projectTypesWithProjectAssignments = [];

				for (var projectType in projectTypesWithProjectAssignments) {
					let projectAssignments = JSON.parse(JSON.stringify(projectTypesWithProjectAssignments[projectType]));
	
					if (projectAssignments) {
						projectAssignments.forEach(projectAssignment => {
							let timesheetEntries = projectAssignment.Timesheet_Entries__r;

							projectAssignment.label = projectAssignment.Project__r.Name + (projectAssignment.Project_Role__r ? ': ' + projectAssignment.Project_Role__r.Name : '');

							if (timesheetEntries) {
								timesheetEntries.forEach(timesheetEntry => {
									let timesheetEntryDate = new Date(timesheetEntry.Timesheet_Date__c);
									let timesheetEntryDateNoTimestamp = new Date(timesheetEntryDate.getFullYear(), timesheetEntryDate.getMonth(), timesheetEntryDate.getDate());
	
									timesheetEntry.formattedTimesheetDate = timesheetEntryDateNoTimestamp;
									timesheetEntry.dayName = DAYS_LIST[timesheetEntryDateNoTimestamp.getDay()];

									if (holidays) {
										holidays.forEach(holiday => {
											let holidayDate = new Date(holiday.ActivityDate);
											let holidayDateNoTimestamp = new Date(holidayDate.getFullYear(), holidayDate.getMonth(), holidayDate.getDate());

											if (timesheetEntryDateNoTimestamp.getTime() == holidayDateNoTimestamp.getTime()) {
												timesheetEntry.isHoliday = true;
											}
										});
									}
								});
								
							} else {
								timesheetEntries = [];
							}
	
							projectAssignment.Timesheet_Entries__r = [].concat(getCurrentWeekTimesheetDates(this.selectedDate, this.numOfDaysToShow, timesheetEntries));
						});
	
						this.projectTypesWithProjectAssignments.push({
							value: projectAssignments, 
							key: projectType
						});
					}
				}
	
				this.error = undefined;
				this.loading = false;
	
				if (callback) {
					callback();
				}
	
			}).catch(error => {
				console.log("error ", error);
	
				this.projectTypesWithProjectAssignments = undefined;
				this.loading = false;
	
				if (callback) {
					callback();
				}
			});
		}).catch(error => {
			console.log("error ", error);

			this.loading = false;

			if (callback) {
				callback();
			}
		});
	}

	subscribeToMessageChannel() {
        this.subscription = subscribe(
            this.messageContext,
            TIMESHEET_ENTRY_WRAPPER_CHANNEL,
            (message) => this.handleMessage(message)
        );
    }

	connectedCallback() {
		this.subscribeToMessageChannel();
		this.refreshData();
	}

	handleMessage(message) {
		this.loading = true;
		this.projectTypesWithProjectAssignments = null;

		if (message) {
			this.numOfDaysToShow = message.numOfDaysToShow;
		}

		this.refreshData(() => {
			console.log('REFRESHED');
		});
	}

	updateArrayRealtime(event) {
		let projectAssignments = event.detail.projectAssignments;
		let allProjectAssignments = [];

		if (this.projectTypesWithProjectAssignments && projectAssignments) {
			this.projectTypesWithProjectAssignments.forEach(projectTypeWithProjectAssignments => {
				projectAssignments.forEach(projectAssignment => {
					if (projectAssignment.Project__r && projectAssignment.Project__r.RecordType && projectTypeWithProjectAssignments.key == projectAssignment.Project__r.RecordType.Name) {
						projectTypeWithProjectAssignments.value = projectAssignments;
						
						return;
					}
				});

				allProjectAssignments = allProjectAssignments.concat(projectTypeWithProjectAssignments.value);
			});
		}

		this.passProjectAssignmentsArray(allProjectAssignments);
    }

	passProjectAssignmentsArray(projectAssignments) {
		let payload = {
			selectedDate: this.selectedDate, 
			numOfDaysToShow: this.numOfDaysToShow, 
			projectAssignments: projectAssignments
		}
		
		publish(this.messageContext, TIMESHEET_ENTRY_TOTAL_HOURS_SUMMARY_CHANNEL, payload);
	}

    saveTimesheetEntries(event) {		
		if (this.validInput()) {
			this.save(false, false, () => {
				this.loading = false;
			});
		}
	}

	saveAndSubmitTimesheetEntries(event) {
		if (this.validInput() && this.validateIfForecastHoursMet()) {
			this.save(true, false, () => {
				this.loading = false;
			});
		}
	}

	saveAndPreviousWeek(event) {
		if (this.validInput()) {
			this.saving = true;
			// this.loading = true;

			this.save(false, true, () => {
				let tempDate = (startOfWeek(this.selectedDate).getTime() - SEVEN_DAYS_IN_MILLISECONDS);			
				let prevWeek = new Date(tempDate);

				this.selectedDate = prevWeek;

				this.refreshSiblingComponents();
			});
		}
	}

	saveAndNextWeek(event) {
		if (this.validInput()) {
			this.saving = true;
			// this.loading = true;

			this.save(false, true, () => {
				let tempDate = (startOfWeek(this.selectedDate).getTime() + SEVEN_DAYS_IN_MILLISECONDS);			
				let nextWeek = new Date(tempDate);

				this.selectedDate = nextWeek;

				this.refreshSiblingComponents();
			});
		}
	}

	goToCurrentDate(event) {
		let dateToday = new Date();
		let dateTodayNoTimestamp = new Date(dateToday.getFullYear(), dateToday.getMonth(), dateToday.getDate());
		let tempDate = new Date(startOfWeek(dateTodayNoTimestamp).getTime());

		this.selectedDate = tempDate;

		this.refreshSiblingComponents();
	}

	refreshSiblingComponents() {
		let payload = {
			selectedDate: this.selectedDate, 
			numOfDaysToShow: this.numOfDaysToShow
		}
		
		publish(this.messageContext, TIMESHEET_ENTRY_WRAPPER_CHANNEL, payload);
		publish(this.messageContext, TIMESHEET_ENTRY_HEADER_CHANNEL, payload);
		publish(this.messageContext, TIMESHEET_ENTRY_TOTAL_HOURS_SUMMARY_CHANNEL, payload);
		publish(this.messageContext, TIMESHEET_ENTRY_TOTAL_HOURS_WRAPPER_CHANNEL, payload);
		fireEvent(this.pageRef, "refreshTimesheetEntriesSummaryTotals", payload);
	}

	validInput() {
		let valid = true;
		let timesheetEntryAccordionComponent = this.template.querySelector('c-timesheet-entry-accordion');

		if (timesheetEntryAccordionComponent) {
			let projectSelectorComponents = timesheetEntryAccordionComponent.getProjectSelectorComponents();

			if (projectSelectorComponents) {
				projectSelectorComponents.forEach(element => {
					valid = valid && element.validateInput();
				});
			}			
		}

		if (!valid) {
			this.dispatchEvent(
				new ShowToastEvent({
					title: 'Error',
					message: 'Please enter comments on your time entries before saving/navigating',
					variant: 'error'
				})
			);
		}

		return valid;
	}

	validateIfForecastHoursMet() {
		let valid = true;
		let timesheetEntryAccordionComponent = this.template.querySelector('c-timesheet-entry-accordion');

		if (timesheetEntryAccordionComponent) {
			let projectSelectorComponents = timesheetEntryAccordionComponent.getProjectSelectorComponents();

			if (projectSelectorComponents) {
				projectSelectorComponents.forEach(element => {
					valid = valid && element.validateIfForecastedHoursMet();
				});
			}
		}

		if (!valid) {
			this.showSubmissionConfirmationDialog = true;
		}

		return valid;
	}

	handleSubmissionConfirmationResponse(event) {
		let confirmationDialogDecision = event.detail.status;

		if (confirmationDialogDecision == 'confirm') {
			this.save(true, false, () => {
				this.loading = false;
				this.showSubmissionConfirmationDialog = false;
			});

		} else if (confirmationDialogDecision == 'cancel') {
			this.showSubmissionConfirmationDialog = false;
		}
	}

	//temp
	getTimesheetEntries(isSubmit) {
		let timesheetEntries = [];
		let filteredTimesheetEntries = [];

		this.saving = true;

		if (this.projectTypesWithProjectAssignments) {
			this.projectTypesWithProjectAssignments.forEach(projectTypeWithProjectAssignments => {
				if (projectTypeWithProjectAssignments.value) {
					projectTypeWithProjectAssignments.value.forEach(projectAssignment => {
						if (projectAssignment.Timesheet_Entries__r) {
							projectAssignment.Timesheet_Entries__r.forEach(timesheetEntry => {
								if (timesheetEntry.isDummy === false) {
									timesheetEntries.push(timesheetEntry);
								}
							});
						}
						
						if (timesheetEntries) {
							if (isSubmit) {
								timesheetEntries.forEach(timesheetEntry => {
									let timesheetEntryDate = new Date(timesheetEntry.Timesheet_Date__c);
									let timesheetEntryDateNoTimestamp = new Date(timesheetEntryDate.getFullYear(), timesheetEntryDate.getMonth(), timesheetEntryDate.getDate());
									let dateToday = new Date();
									let dateTodayNoTimestamp = new Date(dateToday.getFullYear(), dateToday.getMonth(), dateToday.getDate());
				
									if (timesheetEntryDateNoTimestamp <= dateTodayNoTimestamp) {
										timesheetEntry.Locked__c = true;
									}
								});

							}
						}
					});
				}
			});
		}
		
		return timesheetEntries;
	}

	save(isSubmit, navigating, callback) {
		let timesheetEntries = this.getTimesheetEntries(isSubmit);

		upsertTimesheetEntries({
			entries: timesheetEntries

		}).then(result => {
			this.saving = false;

			if (result) {
				if (isSubmit) {
					this.dispatchEvent(
						new ShowToastEvent({
							title: 'Timesheet Entries Submitted',
							message: 'Timesheet Entries have been submitted for approval!',
							variant: 'success'
						})
					);

				} else {
					this.dispatchEvent(
						new ShowToastEvent({
							title: 'Timesheet Entries Saved',
							message: 'Timesheet Entries have been saved successfully!',
							variant: 'success'
						})
					);
				}

			} else {
				console.log('WALANG LAMAN EH');
			}
			
			if (!navigating) {
				this.refreshSiblingComponents();
			}
			
			if (callback) {
				callback();
			}

		}).catch(error => {
			this.dispatchEvent(
				new ShowToastEvent({
					title: 'Error on saving Timesheet Entries',
					message: error,
					variant: 'error'
				})
			);

			if (callback) {
				callback();
			}
		});
	}

}