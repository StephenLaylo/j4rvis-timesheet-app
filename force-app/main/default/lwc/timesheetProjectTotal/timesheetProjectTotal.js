import { api, LightningElement, wire } from 'lwc';
import getTimesheetEntriesSummaryTotals from "@salesforce/apex/TimesheetService.getTimesheetEntriesSummaryTotals";
import userId from "@salesforce/user/Id";
import { CurrentPageReference } from 'lightning/navigation';
import { registerListener, unregisterAllListeners } from "c/pubsub";
import { refreshApex } from '@salesforce/apex';

export default class TimesheetProjectTotal extends LightningElement {

    @api projectRecordType;
    @api selectedDate = new Date();

	numOfDaysToShow = 5;
    totalForecastedHours;
	totalActualHours;
	totalActualHoursPercentage;

    currentUserId = userId;
	selectedTimePeriod = "week";
	totals;

	@wire(CurrentPageReference) 
	pageRef;

    @wire(getTimesheetEntriesSummaryTotals, {
		userId: "$currentUserId",
		startDate: "$selectedDate",
		timeframe: "$selectedTimePeriod",
        recordType: "$projectRecordType", 
		numOfDaysToShow: "$numOfDaysToShow"
	})
    wiredTimesheetEntriesSummaryTotals(value) {
		this.wiredTESummaryTotals = value;
		const { data, error } = value;

		if (data) {
			this.totals = (data != null && data.length > 0) ? data[0] : { forecastedHoursSum: 0, hoursSum: 0 };
			this.totalForecastedHours = (this.totals.forecastedHoursSum && !isNaN(this.totals.forecastedHoursSum)) ? Number(this.totals.forecastedHoursSum) : 0;
			this.totalActualHours = (this.totals.hoursSum && !isNaN(this.totals.hoursSum)) ? Number(this.totals.hoursSum) : 0;

			this.totalActualHoursPercentage = (this.totalActualHours / this.totalForecastedHours) * 100;

			this.totalForecastedHours = (this.totalForecastedHours == null || isNaN(this.totalForecastedHours) ? 0 : this.totalForecastedHours);
			this.totalActualHours = (this.totalActualHours == null || isNaN(this.totalActualHours) ? 0 : this.totalActualHours);
			this.totalActualHoursPercentage = (this.totalActualHoursPercentage == null || isNaN(this.totalActualHoursPercentage) ? 0 : this.totalActualHoursPercentage);

			try {
				this.dValue = this.totalActualHoursPercentage >= 100 ? "M 1 0 A 1 1 0 1 0 1 2.4492935982947064e-16 L 0 0" : "M 1 0 A 1 1 0 " + Math.floor(this.totalActualHoursPercentage / 50) + " 1 " + Math.cos(2 * Math.PI * this.totalActualHoursPercentage / 100) + " " + Math.sin(2 * Math.PI * this.totalActualHoursPercentage / 100) + " L 0 0";
			
			} catch (error) {
				console.log('~~ wiredTimesheetEntriesSummaryTotals -> error', error);
			}
			
			this.error = undefined;
			
		} else if (error) {
			this.errorMessage = error.body.message;
 
			console.log("error " + JSON.stringify(this.errorMessage));
		}

		this.loading = false;
	}

	connectedCallback() {
		registerListener("refreshTimesheetEntriesSummaryTotals", this.refreshTimesheetEntriesSummaryTotals, this);
		registerListener("refreshTimesheetEntriesSummaryTotalsTemp" + this.projectRecordType, this.refreshTimesheetEntriesSummaryTotalsTemp, this);
	}

	disconnectedCallback() {
		unregisterAllListeners(this);
	}

	refreshTimesheetEntriesSummaryTotals(message) {
		if (message) {
			this.selectedDate = new Date(message.selectedDate);
			this.numOfDaysToShow = message.numOfDaysToShow;

			if (this.wiredTESummaryTotals) {
				this.loading = true;

				refreshApex(this.wiredTESummaryTotals).then(() => {
					this.loading = false;
				});
			}
		}
	}

	refreshTimesheetEntriesSummaryTotalsTemp(projectAssignments) {
		let totalActualHours = 0;

		if (projectAssignments) {
			projectAssignments.forEach(projectAssignment => {
				if (projectAssignment.Timesheet_Entries__r) {
					projectAssignment.Timesheet_Entries__r.forEach(timesheetEntry => {
						totalActualHours += timesheetEntry.Hours__c ? timesheetEntry.Hours__c : 0;
					});
				}
			});
	
			this.totalActualHours = totalActualHours;
			this.totalActualHoursPercentage = ((totalActualHours / Number(this.totals.forecastedHoursSum)) * 100);
	
			this.totalActualHours = (this.totalActualHours == null || isNaN(this.totalActualHours) ? 0 : this.totalActualHours);
			this.totalActualHoursPercentage = (this.totalActualHoursPercentage == null || isNaN(this.totalActualHoursPercentage) ? 0 : this.totalActualHoursPercentage);
	
			this.dValue = this.totalActualHoursPercentage >= 100 ? "M 1 0 A 1 1 0 1 0 1 2.4492935982947064e-16 L 0 0" : "M 1 0 A 1 1 0 " + Math.floor(this.totalActualHoursPercentage / 50) + " 1 " + Math.cos(2 * Math.PI * this.totalActualHoursPercentage / 100) + " " + Math.sin(2 * Math.PI * this.totalActualHoursPercentage / 100) + " L 0 0";
		}
	}

	get totalActualHoursRingClass() {
		return 'slds-progress-ring slds-progress-ring_large ' + (this.totalActualHoursPercentage < 100 ? 'slds-progress-ring_warning' : '');
	}
    
}