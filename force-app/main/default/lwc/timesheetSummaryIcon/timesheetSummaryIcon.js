import { LightningElement, api } from 'lwc';

export default class TimesheetSummaryIcon extends LightningElement {

    @api forecastedHours;
    @api actualHours;

    get isSuccess() {
        return (this.forecastedHours > 0 && this.actualHours > 0) && this.actualHours == this.forecastedHours;
    }

    get displayIcon() {
        return this.forecastedHours > 0 && this.actualHours != undefined &&  this.actualHours > 0;
    }

    get hoursCss() {
        return (this.forecastedHours > 0 && this.actualHours > 0) && this.actualHours == this.forecastedHours ? 
            'success-text' : 'warning-text';
    }
    
}