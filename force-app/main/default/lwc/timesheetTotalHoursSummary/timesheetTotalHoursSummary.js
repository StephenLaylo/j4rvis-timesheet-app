import { track, LightningElement, wire } from 'lwc';
import getTimesheetEntriesSummary from "@salesforce/apex/TimesheetService.getTimesheetEntriesSummary";
import getHolidays from "@salesforce/apex/TimesheetService.getHolidays";
import userId from "@salesforce/user/Id";
import { subscribe, MessageContext } from 'lightning/messageService';
import TIMESHEET_ENTRY_TOTAL_HOURS_SUMMARY_CHANNEL from '@salesforce/messageChannel/Timesheet_Entry_Total_Hours_Summary_Update__c';
import { startOfWeek, getCurrentWeekTimesheetDates, ONE_DAY_IN_MILLISECONDS, DAYS_LIST } from "c/commonUtils";
import { refreshApex } from '@salesforce/apex';
import { arrayGroupBy, arrayDistinctTrim } from 'c/commonUtils';

export default class TimesheetTotalHoursSummary extends LightningElement {
    currentUserId = userId;
    selectedDate = new Date();
	numOfDaysToShow = 5;
	selectedTimePeriod = "week";

	@track summaryList;

	loading = true;

	@wire(MessageContext)
    messageContext;

    @wire(getTimesheetEntriesSummary, {
		userId: "$currentUserId",
		startDate: "$selectedDate",
		timeframe: "$selectedTimePeriod", 
		numOfDaysToShow: "$numOfDaysToShow"
	})
    wiredTimesheetEntriesSummary(value) {
		this.wiredTESummary = value;
		const { data, error } = value;

		getHolidays({
			startDate: this.selectedDate,
			timeframe: this.selectedTimePeriod,
			numOfDaysToShow: this.numOfDaysToShow

		}).then(holidays => {
			if (data) {
				this.summaryList = JSON.parse(JSON.stringify(data));
				this.error = undefined;

				if (this.summaryList) {
					this.summaryList.forEach(summaryItem => {
						let timesheetEntryDate = new Date(summaryItem.Timesheet_Date__c);
						let timesheetEntryDateNoTimestamp = new Date(timesheetEntryDate.getFullYear(), timesheetEntryDate.getMonth(), timesheetEntryDate.getDate());

						summaryItem.formattedTimesheetDate = timesheetEntryDateNoTimestamp;
						summaryItem.dayName = DAYS_LIST[timesheetEntryDateNoTimestamp.getDay()];

						if (holidays) {
							holidays.forEach(holiday => {
								let holidayDate = new Date(holiday.ActivityDate);
								let holidayDateNoTimestamp = new Date(holidayDate.getFullYear(), holidayDate.getMonth(), holidayDate.getDate());

								if (timesheetEntryDateNoTimestamp.getTime() == holidayDateNoTimestamp.getTime()) {
									summaryItem.isHoliday = true;
								}
							});
						}
					});

					this.summaryList = [].concat(getCurrentWeekTimesheetDates(this.selectedDate, this.numOfDaysToShow, this.summaryList));

					this.summaryList.forEach(summaryItem => {
						let dateToday = new Date();
						let dateTodayNoTimestamp = new Date(dateToday.getFullYear(), dateToday.getMonth(), dateToday.getDate());

						let timesheetEntryDate = new Date(summaryItem.Timesheet_Date__c);
						let timesheetEntryDateNoTimestamp = new Date(timesheetEntryDate.getFullYear(), timesheetEntryDate.getMonth(), timesheetEntryDate.getDate());
						
						let saturdays = new Date(startOfWeek(timesheetEntryDate).getTime() + (ONE_DAY_IN_MILLISECONDS * 5));
						let saturdaysNoTimestamp = new Date(saturdays.getFullYear(), saturdays.getMonth(), saturdays.getDate());
						
						let sundays = new Date(startOfWeek(timesheetEntryDate).getTime() + (ONE_DAY_IN_MILLISECONDS * 6));
						let sundaysNoTimestamp = new Date(sundays.getFullYear(), sundays.getMonth(), sundays.getDate());

						summaryItem.cssClass = 'slds-p-around_x-small';

						if (timesheetEntryDateNoTimestamp.getTime() == dateTodayNoTimestamp.getTime()) {
							summaryItem.cssClass += ' timesheet-today';

						} else if (summaryItem.isHoliday === true) {
							summaryItem.cssClass += ' timesheet-holiday';

						} else if (timesheetEntryDateNoTimestamp.getTime() == saturdaysNoTimestamp.getTime() || timesheetEntryDateNoTimestamp.getTime() == sundaysNoTimestamp.getTime()) {
							summaryItem.cssClass += ' timesheet-weekend';
						}

						if (summaryItem.isDummy) {
							summaryItem.cssClass += ' slds-theme_shade';
						}
					});
				}

			} else if (error) {
				this.errorMessage = error.body.message;

				console.log("error ", JSON.stringify(this.errorMessage));

				this.projectAssignments = undefined;
			}

		}).catch(error => {
			console.log("error ", error);

			this.loading = false;
		});

		this.loading = false;
	}

	subscribeToMessageChannel() {
        this.subscription = subscribe(
            this.messageContext,
            TIMESHEET_ENTRY_TOTAL_HOURS_SUMMARY_CHANNEL,
            (message) => this.handleMessageTimesheetTotalHoursSummary(message)
        );
    }

	connectedCallback() {
		this.subscribeToMessageChannel();
	}

	handleMessageTimesheetTotalHoursSummary(message) {
		if (message != null) {
			this.refreshTimesheetEntriesSummary(message);
		}
	}

	refreshTimesheetEntriesSummary(message) {
		if (message) {
			if (message.projectAssignments) {
				this.refreshTimesheetEntriesSummaryTemp(message.projectAssignments);

			} else {
				this.loading = true;
				this.selectedDate = new Date(message.selectedDate);
				this.numOfDaysToShow = message.numOfDaysToShow;

				refreshApex(this.wiredTESummary).then(() => {
					this.loading = false;
				});
			}
		}
	}

	refreshTimesheetEntriesSummaryTemp(projectAssignments) {
		let timesheetDates = [];
		let projectAssignmentIds = [];

		this.summaryList = JSON.parse(JSON.stringify(this.summaryList));

		let timesheetEntries = [];

		projectAssignments.forEach(projectAssignment => {
			if (projectAssignment.Timesheet_Entries__r) {
				timesheetEntries = timesheetEntries.concat(projectAssignment.Timesheet_Entries__r);
			}
		});

		timesheetEntries.forEach(timesheetEntry => {
			if (timesheetEntry.Timesheet_Date__c) {
				timesheetDates.push(timesheetEntry.Timesheet_Date__c);
			}

			if (timesheetEntry.Project_Assignment__c) {
				projectAssignmentIds.push(timesheetEntry.Project_Assignment__c);
			}
		})

		timesheetDates = arrayDistinctTrim(timesheetDates);
		projectAssignmentIds = arrayDistinctTrim(projectAssignmentIds);

		timesheetDates.forEach(timesheetDate => {
			let totalActualHours = 0;
			let totalNonBillableHours = 0;
			
			let groupedTimesheetEntriesByDate = arrayGroupBy(timesheetEntries, ['Timesheet_Date__c', 'Project_Assignment__c'])[timesheetDate];
			let summaryListItem = this.summaryList.find(summaryItem => (summaryItem.Timesheet_Date__c == timesheetDate));

			if (groupedTimesheetEntriesByDate) {
				projectAssignmentIds.forEach(projectAssignmentId => {
					let groupedTimesheetEntriesByProjectAssignments = groupedTimesheetEntriesByDate[projectAssignmentId];
					
					if (groupedTimesheetEntriesByProjectAssignments) {
						groupedTimesheetEntriesByProjectAssignments.forEach(timesheetEntry => {
							totalActualHours += timesheetEntry.Hours__c ? timesheetEntry.Hours__c : 0;
							totalNonBillableHours += timesheetEntry.Actual_Non_Billable_Hours__c ? timesheetEntry.Actual_Non_Billable_Hours__c : 0;
						});
					}
				});
			}

			if (summaryListItem) {
				try {
					summaryListItem.hoursSum = totalActualHours;
					summaryListItem.actualNonBillableSum = totalNonBillableHours;
	
				} catch (error) {
					console.log('error', error);
				}
			}
		});
	}

	get numOfDaysColumns() {
		return (this.numOfDaysToShow == 5 ? 'slds-size_1-of-5' : 'slds-size_1-of-7')
	}

	// get hoursCss() {
    //     return (this.forecastedHours > 0 && this.actualHours > 0) && this.actualHours >= this.forecastedHours ? 'success-text' : 'warning-text';
    // }
	
}