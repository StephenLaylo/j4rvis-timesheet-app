import { LightningElement, wire } from 'lwc';
import { publish, subscribe, MessageContext } from 'lightning/messageService';
import TIMESHEET_ENTRY_TOTAL_HOURS_WRAPPER_CHANNEL from '@salesforce/messageChannel/Timesheet_Entry_Total_Hours_Wrapper_Update__c';
import TIMESHEET_ENTRY_TOTAL_HOURS_SUMMARY_CHANNEL from '@salesforce/messageChannel/Timesheet_Entry_Total_Hours_Summary_Update__c';
import TIMESHEET_ENTRY_WRAPPER_CHANNEL from '@salesforce/messageChannel/Timesheet_Entry_Wrapper_Update__c';
import TIMESHEET_ENTRY_HEADER_CHANNEL from '@salesforce/messageChannel/Timesheet_Entry_Header_Update__c';
import { CurrentPageReference } from 'lightning/navigation';
import { fireEvent } from "c/pubsub";

export default class TimesheetTotalHoursWrapper extends LightningElement {

    selectedDate = new Date();
    is5DaysSelected = true;

    @wire(MessageContext)
    messageContext;

	@wire(CurrentPageReference) 
	pageRef;

    subscribeToMessageChannel() {
        this.subscription = subscribe(
            this.messageContext,
            TIMESHEET_ENTRY_TOTAL_HOURS_WRAPPER_CHANNEL,
            (message) => this.handleMessageTimesheetTotalHoursWrapper(message)
        );
    }

    connectedCallback() {
        this.subscribeToMessageChannel();
	}

    handleMessageTimesheetTotalHoursWrapper(message) {
        if (message != null) {
            this.updateSelectedDate(new Date(message.selectedDate));
        }
    }

    updateSelectedDate(selectedDate) {
        this.selectedDate = selectedDate;

        this.refreshSiblingComponents();
    }

    refreshSiblingComponents() {
        let payload = {
			selectedDate: this.selectedDate, 
			numOfDaysToShow: (this.is5DaysSelected ? 5 : 7)
		}

        publish(this.messageContext, TIMESHEET_ENTRY_HEADER_CHANNEL, payload);
        publish(this.messageContext, TIMESHEET_ENTRY_TOTAL_HOURS_SUMMARY_CHANNEL, payload);
        publish(this.messageContext, TIMESHEET_ENTRY_WRAPPER_CHANNEL, payload);
		fireEvent(this.pageRef, "refreshTimesheetEntriesSummaryTotals", payload);
    }

    handleFiveDaysOnClick(event) {
        this.is5DaysSelected = true;

        this.refreshSiblingComponents();
    }

    handleSevenDaysOnClick(event) {
        this.is5DaysSelected = false;

        this.refreshSiblingComponents();
    }

    get fiveDaysButtonVariant() {
        return (this.is5DaysSelected ? 'brand' : 'neutral');
    }

    get sevenDaysButtonVariant() {
        return (!this.is5DaysSelected ? 'brand' : 'neutral');
    }
}